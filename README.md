# Busca de processos na justiça de São Paulo
Esta aplicação consulta processos por CNPJ no site da [justiça de São Paulo](https://esaj.tjsp.jus.br/)
Os dados dos processos são armazenados em PDF.

## Instalação
###Windows: 

1) Faça download do [Python3 para Windows](https://www.python.org/ftp/python/3.7.3/python-3.7.3-amd64.exe) 

*Utilize o Windows PowerShell para executar os passos seguintes:*

2) Instale as dependências especificadas no arquivo "requirements.txt"

`py -m pip install -y requirements.txt`

3) Instale o [WKHTMLTODPF](https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_msvc2015-win64.exe)

### Linux:
1) Certifique-se de que o Python3 e o pip3 estão instalados no sistema:

`$sudo apt install python3 python3-pip`

2) Instale as dependências especificadas no arquivo "requirements.txt"

`pip3 install -y requirements.txt`

3) Instale o wkhtmltopdf

`sudo apt install wkhtmltopdf`

## Utilização
Basta executar o programa através do PowerShell (no Windows) ou terminal (no Linux), fornecendo o número do CNPJ, sem pontuações, como argumento:

`py busca_processos.py NUM_DO_CNPJ` (Windows)

`$ python3 busca_processos.py NUM_DO_CNPJ` (Linux)

Os processos encontrados serão armazenados no formato PDF dentro de um diretório nomeado pelo CNPJ.

## Suporte
Para reportar problemas envie um email para [leonardo@sflabs.com.br](mailto:leonardo@sflabs.com.br) 
