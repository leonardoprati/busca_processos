import ssl
import os
import time
from argparse import ArgumentParser

import urllib
from bs4 import BeautifulSoup
import pdfkit

parser = ArgumentParser(description='')
parser.add_argument('cnpj', type=str, help='num do cnpj a pesquisar')

args = parser.parse_args()

# Workaround para falha de certificado ssl
ssl._create_default_https_context = ssl._create_unverified_context

# Aqui estamos requisitando a página com os resultados para o CNPJ informado
response = urllib.request.urlopen(f'https://esaj.tjsp.jus.br/cpopg/search.do?conversationId=&dadosConsulta.localPesquisa.cdLocal=-1&cbPesquisa=DOCPARTE&dadosConsulta.tipoNuProcesso=UNIFICADO&dadosConsulta.valorConsulta={args.cnpj}&uuidCaptcha=')
html_file = response.fp

# Buscando por links com a classe "linkProcesso", utilizada pelo site para marcar as páginas dos processos
soup = BeautifulSoup(html_file, 'html.parser')
links = soup.find_all('a', class_='linkProcesso', href=True)

# Se encontrou algum link
i = 1
if (len(links) > 0) and not os.path.exists(args.cnpj) :
    os.makedirs(args.cnpj)

    # Para cada link encontrado
    for a in links:
        link = 'https://esaj.tjsp.jus.br' + a['href']
        
        # BUG WORKAROUND: algumas vezes aparece um 2000" impresso, entre linefeeds, no meio do link.
        # Tentou-se substituir "\n2000" por "", porem ainda sobra um linefeed
        # Tentou-se substituir "\n2000\n" por "", mas aí nem o "\n2000" é substituido
        # Infelizmente um link se perde no meio do processo
        if "\n2000" in link:
            pass
        # Abre a página do processo
        else:
            response = urllib.request.urlopen(link)
            html_file = response.fp        
            soup = BeautifulSoup(html_file, 'html.parser')
            
            divs = soup.find_all('div', class_='')
            
            # Busca pela divisão com dados do processo
            for div in divs:
                subtitle = div.find_all('h2')
                for sub in subtitle:
                    if "Dados do processo" in sub.get_text():
                        div = '<head><meta charset="utf-8"></head>' + str(div)
                        div = div + f'<p>{link}<\p>'
                        # Imprime dados do processo em PDF
                        pdfkit.from_string(div, os.path.join(args.cnpj,f'processo_{args.cnpj}_{i}.pdf'))
                        i += 1
